# Séadna - Peadar Ua Laoghaire (1907) #

Edición bilingüe paralela irlandés-castellano.

Si detectas algún fallo en la traducción, o quieres proponer una mejora, puedes indicarlo añadiendo una [incidencia](issues) o mediante un pull request.

## Compilación ##

* Las fuentes están pensadas para ser compiladas con XeTex.
* Se ha usado la tipografía [Gadelica](http://www.iol.ie/~sob/gadelica/) para el texto en irlandés. Si prefieres no usar la tipografía tradicional, ha de desactivarse en el preámbulo del documento.

### pdf ###

Requiere `xelatex`.


```
#!bash
make

```

o

```
#!bash
make pdf
```

### html ###

Requiere [latexml](http://dlmf.nist.gov/LaTeXML/).

Se generan dos archivos, uno para cada lengua.


```
#!bash

make html
```

### epub ###

Requiere [calibre](http://calibre-ebook.com/) (en concreto la herramienta [ebook-meta](http://manual.calibre-ebook.com/cli/ebook-meta.html)) y [latexml](http://dlmf.nist.gov/LaTeXML/).

Se generan dos archivos, uno para cada lengua.

```
#!bash

make epub
```
